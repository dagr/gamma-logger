
# Detector controller for gamma measurements
# Copyright (C) 2020  Norwegain Radiation Protection Authority
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors: Dag Robole,

import os, json, requests
from gc_exceptions import ProtocolError
# from twisted.python import log
from requests.auth import HTTPBasicAuth
from requests.exceptions import Timeout, HTTPError

_api_host = '<reachback rest api>'
_api_username = '<api username>'
_api_password = '<api password>'

def sendSession(log, msg, detector_data, timeout):

	try:		
		url = _api_host + '/sessions'
			
		s = {
			'name':msg['session_name'], 
			'ip_address':msg['ip'], 
			'comment':msg['comment'],
			'livetime': msg['livetime'],
			'detector_data': json.dumps(detector_data)
			}		
		
		response = requests.post(url, auth=HTTPBasicAuth(_api_username, _api_password), timeout=timeout, json=s)
		response.raise_for_status()
		
		log.msg('Reachback, sendSession ok, status: ' + str(response.status_code))
		
		return True
		
	except HTTPError as e:
		log.msg("Reachback, http error: %d - %s" % (response.status_code, str(e)))
	except Timeout:
		log.msg('Reachback, sendSession timeout')
	except Exception as e:
		log.msg("Reachback, exception: %s" % (str(e)))
		
	return False
	
def sendSpectrum(log, spec, timeout):    

	try:
		url = _api_host + '/spectrums'		
			
		s = {
			'session_name':spec['session_name'], 
			'session_index':spec['index'], 
			'start_time':spec['time'],
			'latitude': spec['latitude'],
			'longitude': spec['longitude'],
			'altitude': spec['altitude'],
			'track': spec['track'],
			'speed': spec['speed'],
			'climb': spec['climb'],
			'livetime': spec['livetime'],
			'realtime': spec['realtime'],
			'num_channels': spec['num_channels'],
			'channels': spec['channels'],
			'doserate': spec['doserate']
			}		
		
		response = requests.post(url, auth=HTTPBasicAuth(_api_username, _api_password), timeout=timeout, json=s)
		response.raise_for_status()
		
		log.msg('Reachback, sendSpectrum ok, status: ' + str(response.status_code))
		
	except HTTPError as e:
		log.msg("Reachback, http error: %d - %s" % (response.status_code, str(e)))
	except Timeout:
		log.msg('Reachback, sendSpectrum timeout')	
	except Exception as e:
		log.msg("Exception: %s" % (str(e)))
		
